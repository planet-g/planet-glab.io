function widgetPullup(){
    let widgetContainer = $o("#discord-widget-container");
    let pos = widgetContainer.getAttribute("pos");

    if (pos == "up"){
        widgetContainer.setAttribute("pos", "down");
    } else {
        widgetContainer.setAttribute("pos", "up");
    }
}

function widgetLeave(){
    $o("#discord-widget-container").setAttribute("pos", "down");
}

function rQ(){ //Some arbitiary query
    return ""//"?q=" + Date.now();
}

var baseApp = { path:"apps", childApps:[], type:"more-apps", parent:"apps"}
var apps = {"apps": baseApp}

var appsData = $fuwuProxy();
appsData.currentApp;
appsData.isOpen = false;

var titleData = $fuwuProxy();
titleData.text = "Planet-T";
titleData.image = "img/saturn shine.webp";
titleData.visible = false;

var preloadData = $fuwuProxy();
preloadData.ready = false;
preloadData.images = [];

var appsToLoad = 1;

function loadAppsList(appsArray, parentPath){

    let parentApp = {}

    if (parentPath == "apps"){
        parentApp = baseApp;
    } else {
        parentApp = apps[parentPath];
        if (!("childApps" in parentApp)){
            parentApp.childApps = []
        }
    }

    appsArray.forEach(app => {
        if (typeof app == "string"){
            parentApp.childApps.push(app);
            appsToLoad++;
            $json.get(app + "/app.json" + rQ(), (appJson) => {
                appJson.parent = parentPath
                apps[app] = appJson
                typeChecks(appJson, app)
                oneLoaded();
            })
        } else if (typeof app == "object") {
            let appPath = parentPath + "//" + app.name;
            parentApp.childApps.push(appPath);
            app.parent = parentPath;
            apps[appPath] = app;
            typeChecks(app, appPath)
        }
    });
}

function typeChecks(app, appPath){
    app.path = appPath
    if (app.type == "more-apps"){
        loadAppsList(app.content, appPath)
    } if (app.type == "html-file"){
        appsToLoad++;
        $http.get(app.content + rQ(), (resp)=>{
            app.type = "html";
            app.content = resp;
            oneLoaded();
        })
    } if (app.type == "text-file"){
        appsToLoad++;
        $http.get(app.content + rQ(), (resp)=>{
            app.type = "text";
            app.content = resp;
            oneLoaded();
        })
    }
    if (app.icon){
        preloadData.images.push(app.icon);
    }
}

function oneLoaded(){
    appsToLoad--;
    if (appsToLoad == 0){
        loadHashApp();
    }
}

function loadHashApp(){
    let hash = decodeURI(window.location.hash);
    if (hash.startsWith("#")){
        hash = hash.substring(1);
    }
    let app = apps[hash]
    if (app != null){
        appsData.isOpen = true;
        clickApp(app);
    } else if (hash == ""){
        backClick();
    }
}

window.addEventListener("hashchange", loadHashApp);

function clickApp(app){
    if (app.type == "link"){
        openLink(app.content);
    } else if (app.type != null) {
        appHash(app);
        appsData.currentApp = app;
    }
}

function appIconAdd(icon, index){
    setTimeout(() => icon.setAttribute("visible", "true"), 50 + index * 75);
}

function titleCharAdd(charSpan, index){
    setTimeout(() => charSpan.setAttribute('visible', 'true'), 75 + index * 75)
}

function titleChange(){
    setTimeout(() => titleDone = true, titleData.text.length * 75)
}

function appHash(app){
    window.location.hash = app.path;
}

function openLink(link){
    window.open(link, "_blank");
}

function backClick(){
    appsData.isOpen = false;
    if (window.location.hash != ""){
        window.location.hash = ""
    }
}

function openApps(){
    if (appsToLoad != 0){
        return;
    }
    appsData.currentApp = baseApp;
    appsData.isOpen = true;
    window.location.hash = baseApp.path;
}

var titleDone = true;

const secretTitles = [
    {img:"img/saturn shine.webp",title:"Planet-T"},
    {img:"img/zero.webp",title:"Planet 0"},
    {img:"img/plane-t.webp",title:"Plane T"},
    {img:"img/plan-b.webp",title:"Plan B"},
    {img:"img/planet-earth.webp",title:"Planet Earth"},
    {img:"img/plant-t.webp",title:"Plant T"},
    {img:"img/star-t.webp",title:"Star T"},
]

function randomTitle(pick){
    if (!titleDone){
        return
    }

    let planet = $o("#planet-t");

    if (pick == null){
        do {
            pick = secretTitles[secretTitles.length * Math.random() | 0]
        } while (pick.title == titleData.text)
    }
    

    if (pick.img == undefined){
        pick.img = secretTitles[0].img
    }

    if (pick.img != planet.getAttribute("src")){
        planet.classList.add("appear")
        
        setTimeout(() => {
            if (planet.complete){
                planet.classList.remove("appear")
            }
        }, 100)
    }

    titleData.text = pick.title;
    titleData.image = pick.img;

    titleDone = false;
}

const shootingStarIframe = '<iframe style="width: 100%; height: 100%" src="https://www.youtube-nocookie.com/embed/O-MQC_G9jTU?controls=1&autoplay=1" title="YouTube video player" frameborder="0" allow="autoplay; encrypted-media; picture-in-picture" allowfullscreen></iframe>'

function shootingStar(){
    appsData.isOpen = true;
    appsData.currentApp = {
        content : shootingStarIframe,
        type : "html",
        parent : baseApp.path
    }
    randomTitle({
        img:"img/shooting-star.webp",
        title: "Shooting Star"
    })
}

const baseWidth = 6480 //3240
const baseHeight = 2880 //1440
const baseRatio = (baseWidth + 0.0) / (baseHeight + 0.0)

function whenResized(){
    const realWidth = document.body.clientWidth
    const realHeight = document.body.clientHeight
    const realRatio = (realWidth + 0.0) / (realHeight + 0.0)

    let useRatio = 1.0
    if (realRatio > baseRatio){
        useRatio = (realWidth + 0.0) / (baseWidth + 0.0)
    } else {
        useRatio = (realHeight + 0.0) / (baseHeight + 0.0)
    }

    repositionClickable($o("#small-planet"), 925, 255, 300, useRatio)
    //1400 2760
    repositionClickable($o("#bright-star"), -1840, 1325, 40, useRatio)
    //5425 660
    repositionClickable($o("#shooting-star"), 2190, -780, 70, useRatio)
}

function repositionClickable(el, x, y, width, ratio){
    el.style.left = "calc(50% + " + ((ratio * x) | 0) + "px)"
    el.style.top = "calc(50% + " + ((ratio * y) | 0) + "px)"
    el.style.width = ((ratio * width) | 0) + "px"
    el.style.height = el.style.width
}

$domReady(() => {
    
    function whenLoaded(){
        titleData.visible = true;
        setTimeout(() => preloadData.ready = true, 100);
    }

    if (document.readyState != "complete"){
        window.addEventListener('load', whenLoaded)
    } else {
        setTimeout(whenLoaded, 100)
    }

    appsToLoad = 1;
    $json.get("apps.json" + rQ(), (json) => {
        loadAppsList(json.apps, "apps");
        oneLoaded();
    })

    window.addEventListener('resize', whenResized)
    whenResized()
})